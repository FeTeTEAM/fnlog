<?php

require('db.fun.php');

fun();
class db{
    function __construct(){
        $this->mysqli = new mysqli($db_host,$db_username,$db_password,$db_sqlname);
        
        if ($this->mysqli->connect_error) {
            die('Connect Error (' . $this->mysqli->connect_errno . ') '
                    . $this->mysqli->connect_error);
        }
    }
    function query( $sql ){
        return $this->mysqli->query( $sql );
    }
    
}

?>
