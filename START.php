<?php
// +---------------------------------------------------------------------
// | FNLOG 1.0  [ FeTe!TEAM ]
// +----------------------------------------------------------------------
// | Copyright © 2015~2018 FETEGZS All rights reserved.
// +----------------------------------------------------------------------
// | FCOS 1.1 [开发版]
// +----------------------------------------------------------------------
// | Author: HanFen <tancan137@foxmail.com>
// +----------------------------------------------------------------------

//前台的硬盘路径
define("APP_PATH", dirname(__FILE__));
//后台的硬盘路径
define("ADM_PATH", APP_PATH.'/admin');
//System的硬盘路径
define("SYSTEM_PATH", APP_PATH.'/System');

//加载数据库配置
include(APP_PATH.'/config/db.config.php');
//加载数据库类
require(SYSTEM_PATH.'/lib/db.class.php');
$db = new db();

