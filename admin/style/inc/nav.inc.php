<div class="side-nav">
		<div class="side-logo">
			<div class="logo">
				<span class="logo-ico">
					<i class="i-l-1"></i>
					<i class="i-l-2"></i>
					<i class="i-l-3"></i>
				</span>
				<strong>FNLOG 控制中心</strong>
			</div>
		</div>
		
		<nav class="side-menu content mCustomScrollbar" data-mcs-theme="minimal-dark">
			<h2>
				<a href="index.html" class="InitialPage"><i class="fa fa-dashboard"></i> 仪表盘</a>
        </h2>
			<ul>
				<li>
					<dl>
						<dt>
							<i class="fa fa-newspaper-o"></i> 文章<i class="icon-angle-right"></i>
						</dt>
           <dd>
			<a href="layer.html"><i class="fa fa-pencil-square-o"></i> 编写文章</a>
					 </dd>
				  <dd>
     <a href="layer.html"><i class="fa fa-wpforms"></i> 全部文章</a>
					 </dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>
							<i class="fa fa-drivers-license-o"></i> 用户管控<i class="icon-angle-right"></i>
						</dt>
						<dd>
							<a href="flex-layout.html"><i class="fa fa-user-circle-o"></i> 管理员-管控</a>
						</dd>
						<dd>
							<a href="flow-layout.html"><i class="fa fa-user-plus"></i> 管理员-添加</a>
						</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>
							<i class="fa fa-object-group"></i> 全局管理<i class="icon-angle-right"></i>
						</dt>
						<dd>
							<a href="button.html"><i class="fa fa-map-signs"></i> 站点设置</a>
						</dd>
          <dd>
							<a href="button.html"><i class="fa fa-exchange"></i> 友链设置</a>
					</dl>
				</li>
				<li>
					<dl>
						<dt>
							<i class="fa fa-trello"></i> 站长工具<i class="icon-angle-right"></i>
						</dt>
						<dd>
							<a href="table.html"><i class="fa fa-get-pocket"></i> 安全中心</a>
						</dd>
           <dd>
							<a href="table.html"><i class="fa fa-rocket"></i> 清理加速</a>
						</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>
							<i class="fa fa-modx"></i> 个性主题<i class="icon-angle-right"></i>
						</dt>
						<dd>
							<a href="form.html"><i class="fa fa-th-large"></i> 主题管理</a>
						</dd>
					</dl>
				</li>
		   <li>
					<dl>
						<dt>
							<i class="fa fa-plug"></i> 插件扩展<i class="icon-angle-right"></i>
						</dt>
						<dd>
							<a href="echarts.html"><i class="fa fa-puzzle-piece"></i> 系统模块</a>
						</dd>
						<dd>
							<a href="echarts.html"><i class="fa fa-futbol-o"></i> 插件管理</a>
						</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>
							<i class="fa fa-shopping-bag"></i> 应用中心<i class="icon-angle-right"></i>
						</dt>
						<dd>
							<a href="popups.html"><i class="fa fa-shopping-bag"></i> 应用中心</a>
						</dd>
						<dd>
							<a href="popups.html"><i class="fa fa-plug"></i> 插件下载</a>
						</dd>
						<dd>
							<a href="popups.html"><i class="fa fa-modx"></i> 主题下载</a>
						</dd>
					</dl>
				</li>
				<li>
					<dl>
						<dt>
							<i class="fa fa-crosshairs"></i> 更多设置<i class="icon-angle-right"></i>
						</dt>
						<dd>
							<a href="ueditor.html"><i class="fa fa-info-circle"></i> 使用手册</a>
						</dd>
					</dl>
				</li>
			
        <li>
					 <dl>
						 <dt>
		         <i class="fa fa-lastfm-square"></i> 系统中心<i class="icon-angle-right"></i>
						</dt>
						<dd>
				  <a href="layer.html"><i class="fa fa-arrow-circle-up"></i>  系统更新</a>
						</dd>
           <dd>
				 <a href="layer.html"><i class="fa fa-lastfm"></i>  系统信息</a>
						</dd>
           <dd>
				  <a href="layer.html"><i class="fa fa-bug"></i>  BUG反馈</a>
						</dd>
					</dl>
				</li>
			</ul>
		</nav>
		<footer class="side-footer">© 2017 FNLOG 1.0</footer>
	</div>
	<div class="content-wrap">
		<header class="top-hd">
			<div class="hd-lt">
				<a class="icon-reorder"></a>
			</div>
			<div class="hd-rt">
				<ul>
					<li>
						<a href="#" target="_blank"><i class="icon-home"></i>前台访问</a>
					</li>
					<li>
						<a><i class="icon-random"></i>清除缓存</a>
					</li>
					<li>
						<a><i class="icon-user"></i>管理员:<em>DeathGhost</em></a>
					</li>
					<li>
						<a><i class="icon-bell-alt"></i>系统消息</a>
					</li>
					<li>
						<a href="javascript:void(0)" id="JsSignOut"><i class="icon-signout"></i>安全退出</a>
					</li>
				</ul>
			</div>
		</header>